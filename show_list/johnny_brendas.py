from datetime import datetime

from bs4 import BeautifulSoup, Tag

from show_list.shows import HttpResponse, Show


show_url = "https://johnnybrendas.com/events/"


VENUE_NAME = "Johnny Brenda's"


def get_show_from_calendar_event(tag: Tag) -> Show:
    title = tag.find(id="eventTitle")
    assert isinstance(title, Tag)

    ticket_url = title.get("href")
    assert isinstance(ticket_url, str)

    headliner_el = title.find("h2")
    assert isinstance(headliner_el, Tag)
    headliner = headliner_el.text.strip()

    date_el = tag.find(id="eventDate")
    assert isinstance(date_el, Tag)

    today = datetime.today()
    show_date_text = f"{date_el.text.strip()} {today.year}"
    if any(i in show_date_text for i in {"June", "July", "May"}):
        show_date = datetime.strptime(show_date_text, "%a, %B %d %Y")
    else:
        if "Sept" in show_date_text:
            show_date_text = show_date_text.replace("Sept", "Sep")
        show_date = datetime.strptime(show_date_text, "%a, %b %d %Y")
    time_container = tag.select(".eventDoorStartDate")[0]
    assert isinstance(time_container, Tag)
    time_el = list(
        el
        for el in time_container.contents
        if isinstance(el, Tag) and el.text.strip() != ""
    )[0]
    assert isinstance(time_el, Tag)
    time_text = time_el.text.strip().replace("Doors: ", "")
    if ":" in time_text:
        time = datetime.strptime(time_text, "%I:%M%p")
    else:
        time = datetime.strptime(time_text, "%I%p")
    return Show(
        headliner=headliner,
        support="",
        start_time_local=datetime(
            today.year, show_date.month, show_date.day, time.hour
        ),
        venue_name=VENUE_NAME,
        ticket_url=ticket_url,
        age_limit=21,
        source=show_url,
    )


def get_shows_from_response(response: HttpResponse) -> list[Show]:
    root = BeautifulSoup(response.content, "html.parser")
    return [
        get_show_from_calendar_event(tag) for tag in root.select(".eventMainWrapper")
    ]
