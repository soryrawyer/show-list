"""
scrape show lists and save results to sqlite
"""

import time

from dataclasses import dataclass
from datetime import datetime
from typing import Protocol, runtime_checkable

import requests


CREATE_SHOWS_TABLE = """
create table if not exists shows (
  id integer primary key,
  headliner text,
  support text,
  start_time_local int,
  venue_name text,
  ticket_url text,
  age_limit int,
  source text,
  created_at int
)
"""
CREATE_SHOWS_INDEX = (
    "create unique index shows_unique on shows(headliner, start_time_local, venue_name)"
)

INSERT_SHOWS = (
    "insert into shows"
    " (headliner, support, start_time_local, venue_name, ticket_url, age_limit, source, created_at)"
    " values (?, ?, ?, ?, ?, ?, ?, datetime('now'))"
    " on conflict(headliner, start_time_local, venue_name)"
    " do update set headliner=excluded.headliner, support=excluded.support,"
    " start_time_local=excluded.start_time_local, venue_name=excluded.venue_name, source=excluded.source"
)


BASE_REQUEST_HEADERS = {"user-agent": "show-list/0.0.1"}


class HttpResponse(Protocol):
    def raise_for_status(self): ...

    def json(self) -> dict: ...

    @property
    def content(self) -> bytes: ...


def datetime_to_unix(dt: datetime) -> int:
    return int(time.mktime(dt.timetuple()))


type SqliteShow = tuple[str, str, int, str, str, int, str]


def show_dict_from_sqlite(row: SqliteShow):
    (
        headliner,
        support,
        start_time_local,
        venue_name,
        ticket_url,
        age_limit,
        source,
    ) = row
    result = {
        "headliner": headliner,
        "start_time_local": start_time_local,
        "ticket_url": ticket_url,
        "venue_name": venue_name,
        "source": source,
    }
    if support is not None:
        result["support"] = support
    if age_limit is not None:
        result["age_limit"] = age_limit
    return result


def show_str_from_sqlite(row: SqliteShow) -> str:
    return Show.from_sqlite(row).to_string()


@dataclass
class Show:
    headliner: str
    support: str | None
    start_time_local: datetime
    venue_name: str
    ticket_url: str
    age_limit: int | None
    source: str

    def to_dict(self) -> dict:
        result = {
            "headliner": self.headliner,
            "start_time_local": datetime_to_unix(self.start_time_local),
            "venue_name": self.venue_name,
            "ticket_url": self.ticket_url,
        }
        if self.support is not None:
            result["support"] = self.support
        if self.age_limit is not None:
            result["age_limit"] = self.age_limit
        return result

    def to_string(self):
        result = f"{self.headliner} "
        if self.support is not None:
            result += f"with {self.support} "
        result += f"on {self.start_time_local.strftime('%Y-%m-%d')} at {self.start_time_local.strftime('%H:%M')}"
        return result

    def to_sqlite(self) -> SqliteShow:
        return (
            self.headliner,
            self.support if self.support is not None else "",
            datetime_to_unix(self.start_time_local),
            self.venue_name,
            self.ticket_url,
            self.age_limit if self.age_limit is not None else 0,
            self.source,
        )

    @classmethod
    def from_sqlite(cls, row: SqliteShow) -> "Show":
        (
            headliner,
            support,
            start_time_local,
            venue_name,
            ticket_url,
            age_limit,
            source,
        ) = row
        return cls(
            headliner,
            support if support != "" else None,
            datetime.fromtimestamp(start_time_local),
            venue_name,
            ticket_url,
            age_limit if age_limit != 0 else None,
            source,
        )


@runtime_checkable
class ShowLister(Protocol):
    show_url: str

    def get_shows_from_response(self, response: HttpResponse) -> list[Show]: ...


def get_shows_from_lister(venue: ShowLister) -> list[Show]:
    resp = requests.get(venue.show_url, headers=BASE_REQUEST_HEADERS)
    resp.raise_for_status()
    return venue.get_shows_from_response(resp)


def get_shows(show_listers: list[ShowLister]) -> list[Show]:
    result = []
    for lister in show_listers:
        result.extend(get_shows_from_lister(lister))
    return result
