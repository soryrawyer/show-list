# 4333 collective
import re

from datetime import datetime

from bs4 import BeautifulSoup, Tag

from show_list.shows import HttpResponse, Show


show_url = "https://4333collective.net/gigs"


def get_show_from_element(event) -> Show:
    title_container = event.select(".eventlist-title-link")
    assert isinstance(title_container, list) and len(title_container) == 1
    title_el = title_container[0]
    assert isinstance(title_el, Tag)
    title = title_el.text

    month = event.select(".eventlist-datetag-startdate--month")[0].text
    day = event.select(".eventlist-datetag-startdate--day")[0].text
    year = datetime.today().year
    show_date_text = f"{year} {month} {day}"
    if any(i in show_date_text for i in {"June", "July", "May"}):
        show_date = datetime.strptime(show_date_text, "%Y %B %d")
    else:
        if "Sept" in show_date_text:
            show_date_text = show_date_text.replace("Sept", "Sep")
        show_date = datetime.strptime(show_date_text, "%Y %b %d")

    venue = event.select(".eventlist-meta-address")[0].text.replace("(map)", "").strip()
    ticket_url = event.select(".sqs-block-button-element")[0].get("href")
    details_section = list(
        tag
        for tag in event.select(".sqs-html-content")[0].children
        if isinstance(tag, Tag)
    )
    age_details = details_section[0].text
    if restriction := re.findall("([0-9]+)\\+", age_details):
        age_limit = int(restriction[0])
    else:
        age_limit = None
    door_time = details_section[1].text.replace("Doors at ", "")
    if ":" in door_time:
        time = datetime.strptime(door_time, "%I:%M%p")
    else:
        time = datetime.strptime(door_time, "%I%p")
    return Show(
        headliner=title,
        support="",
        start_time_local=datetime(
            show_date.year, show_date.month, show_date.day, time.hour
        ),
        venue_name=venue,
        ticket_url=ticket_url,
        age_limit=age_limit,
        source=show_url,
    )


def get_shows_from_response(response: HttpResponse) -> list[Show]:
    root = BeautifulSoup(response.content, "html.parser")
    result = []
    for tag in root.select(".eventlist-event--upcoming"):
        try:
            result.append(get_show_from_element(tag))
        except IndexError:
            print("issue parsing a show from 4333 collective")
            print(tag.select(".eventlist-title-link")[0].text)
    return result
