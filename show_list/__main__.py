import argparse
import json
import os
import sqlite3

from show_list import shows, collective, johnny_brendas, r5_productions, underground_arts, union_transfer

conn = sqlite3.connect(os.environ["SQLITE_PATH"])

parser = argparse.ArgumentParser()
parser.add_argument("-l", "--list", action="store_true")
parser.add_argument(
    "-j", "--json", action="store_true", help="output the show list in json form"
)
args = parser.parse_args()


SHOW_LISTERS: list[shows.ShowLister] = [
    union_transfer,
    r5_productions,
    johnny_brendas,
    collective,
    underground_arts,
]


for lister in SHOW_LISTERS:
    if not args.list:
        # TODO: use a threadpool here?
        data = list(s.to_sqlite() for s in shows.get_shows_from_lister(lister))
        with conn:
            # TODO: instead of writing shows one lister at a time,
            # let's get one list of all shows and then insert them.
            # that would give us a chance to deduplicate shows
            # for example, R5 has shows from johnny brenda's, so we
            # should prefer the Show object coming from the JBs lister
            # although, we might be more information from R5's website...
            # much to think about
            conn.executemany(shows.INSERT_SHOWS, data)

total_shows: list[dict] = []
for row in conn.execute("select distinct venue_name from shows order by venue_name"):
    (venue_name,) = row
    line_transform = shows.show_str_from_sqlite
    if args.json:
        line_transform = shows.show_dict_from_sqlite

    show_list = []
    for show in conn.execute(
        (
            "select headliner, support, start_time_local, venue_name, ticket_url,"
            " age_limit, source"
            " from shows "
            " where venue_name = ? and start_time_local >= strftime('%s', 'now')"
            " order by start_time_local asc"
        ),
        (venue_name,),
    ):
        show_list.append(line_transform(show))
    if args.json:
        total_shows.extend(show_list)
    else:
        print(f"coming up at {venue_name}:")
        print("\n".join(show_list) + "\n")

if args.json:
    print(json.dumps(sorted(total_shows, key=lambda s: s["start_time_local"])))
