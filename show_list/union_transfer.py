from datetime import datetime

from show_list.shows import HttpResponse, Show

show_url = "https://aegwebprod.blob.core.windows.net/json/events/289/events.json"


def show_from_obj(obj) -> Show:
    support = obj["title"]["supportingText"]
    if support is not None:
        support = support.strip()
    return Show(
        headliner=obj["title"]["headlinersText"].strip(),
        support=support if support != "" else None,
        start_time_local=datetime.strptime(obj["eventDateTime"], "%Y-%m-%dT%H:%M:%S"),
        venue_name=obj["venue"]["title"],
        ticket_url=obj["ticketing"]["url"].strip(),
        age_limit=None,
        source=show_url,
    )


def get_shows_from_response(response: HttpResponse) -> list[Show]:
    result = []
    for obj in response.json()["events"]:
        try:
            result.append(show_from_obj(obj))
        except ValueError as err:
            print("uh oh!")
            print(obj["title"]["headlinersText"])
            print(err)
            continue
    return result
