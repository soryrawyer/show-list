from datetime import datetime

from bs4 import BeautifulSoup, Tag

from show_list.shows import HttpResponse, Show


show_url = "https://r5productions.com/events/"


def get_show_from_element(element: Tag) -> Show:
    year = int(
        list(
            i
            for i in reversed(list(element.previous_siblings))
            if isinstance(i, Tag) and "rhp-events-list-separator-month" in i["class"]
        )[0].text.split(" ")[1]
    )

    headliner = element.select("#eventTitle")[0].text.strip()
    support = element.select("#evSubHead")[0].text.strip()
    venue_name = element.select(".rhpVenueContent")[0].text.strip()
    start_time_text = (
        element.select(".eventDoorStartDate")[0]
        .text.strip()
        .split(" | ")[1]
        .replace("Show: ", "")
    )
    if ":" in start_time_text:
        start_time = datetime.strptime(start_time_text, "%I:%M%p")
    else:
        start_time = datetime.strptime(start_time_text, "%I%p")
    show_date_text = element.select("#eventDate")[0].text.strip()
    if any(i in show_date_text for i in {"June", "July", "May"}):
        show_date = datetime.strptime(show_date_text, "%a, %B %d")
    else:
        if "Sept" in show_date_text:
            show_date_text = show_date_text.replace("Sept", "Sep")
        show_date = datetime.strptime(show_date_text, "%a, %b %d")
    ticket_a_element = element.select(".rhp-event-cta")[0].a
    assert isinstance(ticket_a_element, Tag)
    ticket_url = ticket_a_element["href"]
    assert isinstance(ticket_url, str)
    age_limit = None

    start_time_local = datetime(
        year, show_date.month, show_date.day, start_time.hour, start_time.minute
    )
    return Show(
        headliner=headliner,
        support=support,
        start_time_local=start_time_local,
        venue_name=venue_name,
        ticket_url=ticket_url,
        age_limit=age_limit,
        source=show_url,
    )


def get_shows_from_response(response: HttpResponse) -> list[Show]:
    root = BeautifulSoup(response.content, "html.parser")
    return [get_show_from_element(event) for event in root.select(".eventWrapper")]
