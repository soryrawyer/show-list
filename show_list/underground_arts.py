from datetime import datetime

from bs4 import BeautifulSoup

from show_list.shows import HttpResponse, Show

show_url = "https://undergroundarts.org/api/plot/v1/listings"

VENUE_NAME = "Underground Arts"


def show_from_object(event) -> Show | None:
    # TODO: if this is an entry for one show (i.e. not an entry for a multi-day pass)
    # then the dateTime value will look like this:
    # <span>Thu, Jan 23</span><p class="listing-doors listingMeta meta">Doors: 6:30pm •  Show:  7:30pm</p>
    datetime_el = BeautifulSoup(event["dateTime"])
    show_date_text = datetime_el.select("span")[0].text

    if " - " in show_date_text:
        return None

    # TODO: might need to use the ol "if it's september, replace the text" logic
    show_date = datetime.strptime(show_date_text, "%a, %b %d")
    today = datetime.today()
    show_start_text = datetime_el.select(".listing-doors")[0].text.split("Show:")[-1].strip()
    if ":" in show_start_text:
        show_start = datetime.strptime(show_start_text, "%I:%M%p")
    else:
        show_start = datetime.strptime(show_start_text, "%I%p")
    support = ""
    if "lineup" in event and "standard" in event["lineup"]:
        support = ", ".join(band["title"] for band in event["lineup"]["standard"] )
    return Show(
        headliner=event["title"],
        support=support,
        start_time_local=datetime(
            today.year, show_date.month, show_date.day, show_start.hour, show_start.minute
        ),
        venue_name=VENUE_NAME,
        ticket_url=event["ticket"]["link"],
        # age limit will be in the value for 'description'
        age_limit=21,
        source=show_url,
    )


def get_shows_from_response(response: HttpResponse) -> list[Show]:
    result = []
    for event in response.json():
        show = show_from_object(event)
        if show is not None:
            result.append(show)
    return result
