from datetime import datetime

from bs4 import BeautifulSoup

from show_list import shows, r5_productions, union_transfer, collective


def test_ut_obj_to_show(ut_show_obj):
    got = union_transfer.show_from_obj(ut_show_obj)
    want = shows.Show(
        headliner="DJ Shadow",
        support="Holly",
        start_time_local=datetime(2024, 2, 8, 20),
        venue_name="Union Transfer",
        ticket_url="https://www.axs.com/events/510037/dj-shadow-tickets",
        age_limit=None,
        source="test",
    )
    assert got == want


def test_r5_element_to_show(r5_event_card):
    root = BeautifulSoup(r5_event_card, "html.parser")
    got = r5_productions.get_show_from_element(root.select(".eventWrapper")[0])
    want = shows.Show(
        headliner="Waxahatchee",
        support="Tim Heidecker, Gladie",
        start_time_local=datetime(2024, 9, 7, 20),
        venue_name="The Fillmore",
        ticket_url="https://www.ticketmaster.com/event/02005F8FBA825713",
        age_limit=None,
        source="test",
    )
    assert got == want


def test_4333_collective_to_show(collective_4333_show):
    root = BeautifulSoup(collective_4333_show, "html.parser")
    got = collective.get_show_from_element(root)
    want = shows.Show(
        headliner = "Le Siren, Reese Florence, and Milk Dud",
        support="",
        start_time_local=datetime(2025, 1, 9, 19),
        venue_name="Ortlieb's",
        ticket_url="https://link.dice.fm/K5752be02471",
        age_limit=21,
        source="test",
    )
    assert got == want
