import pytest


@pytest.fixture(scope="module")
def ut_show_obj():
    return {
        "active": True,
        "additionalDates": [],
        "age": None,
        "announceDateTime": "2023-10-30T12:00:00",
        "announceDateTimeTimeZone": "America/New_York",
        "announceDateTimeUTC": "2023-10-30T16:00:00",
        "associations": {
            "additionalPerformers": [],
            "headliners": [
                {
                    "active": True,
                    "aliases": None,
                    "bio": "<p>For over 30 years, Josh Davis has "
                    "expressed his passion, taste, and "
                    "values through the music</p>\r\n"
                    "<p>he creates as DJ Shadow. The name "
                    "alone evokes a high watermark for "
                    "instrumental hip-hop</p>\r\n"
                    "<p>and composition. From his first "
                    "masterpiece Endtroducing&hellip;, to "
                    "the genre-hopping UNKLE</p>\r\n"
                    "<p>release Psyence Fiction, to the "
                    "otherworldly elegance of The Private "
                    "Press and its iconic single</p>\r\n"
                    "<p>&ldquo;Six Days,&rdquo; to his "
                    "underrated Bay Area celebration The "
                    "Outsider, his work in the &lsquo;90s "
                    "and first</p>\r\n"
                    "<p>decade of the 2000s is as "
                    "essential as it is hard-to-pin-down. "
                    "In the 2010s, Shadow released</p>\r\n"
                    "<p>the sprawling The Less You Know, "
                    "the Better, with its muscular forays "
                    "into rock music, and</p>\r\n"
                    "<p>closed the decade with The "
                    "Mountain Will Fall and Our Pathetic "
                    "Age, both very ambitious,</p>\r\n"
                    "<p>risk-taking albums that boasted "
                    "some of his best rap collaborations "
                    "by working with Run the</p>\r\n"
                    "<p>Jewels, Nas, and De La Soul, "
                    "among others. If there&rsquo;s a "
                    "single red thread across this "
                    "career, it's</p>\r\n"
                    "<p>his restless ear, always "
                    "searching to rescue some forgotten "
                    "gem from the dustbin of music</p>\r\n"
                    "<p>history or a fresh blast of sound "
                    "from the cutting edge.</p>\r\n"
                    "<p>It&rsquo;s a practice Shadow "
                    "learned as a young boy, enamored "
                    "with collecting comic books, "
                    "baseball</p>\r\n"
                    "<p>cards, 12-inch records, and "
                    "precious songs captured to cassette "
                    "from the radio airwaves late "
                    "at</p>\r\n"
                    "<p>night in northern California. "
                    "With great seriousness and love, he "
                    "will describe the second-hand</p>\r\n"
                    "<p>transistor AM radio on which he "
                    "first heard Grandmaster Flash and "
                    "the Furious Five&rsquo;s "
                    "&ldquo;The</p>\r\n"
                    "<p>Message,&rdquo; when he was 10 "
                    "years old. Or his mind-expanding "
                    "encounter with Public "
                    "Enemy&rsquo;s</p>\r\n"
                    "<p>&ldquo;Rebel Without a "
                    "Pause&rdquo; and that single&rsquo;s "
                    "howling saxophone blast. At the "
                    "heart of DJ Shadow&rsquo;s</p>\r\n"
                    "<p>musical project is deep "
                    "listening. &ldquo;I&rsquo;m always "
                    "trying to please or impress the most "
                    "advanced</p>\r\n"
                    "<p>listener,&rdquo; he says. The "
                    "type of aficionado who will respect "
                    "the layers upon layers of synth "
                    "that</p>\r\n"
                    "<p>went into making a sound that "
                    "can&rsquo;t be readily identified "
                    "but will make you want to share "
                    "the</p>\r\n"
                    "<p>track with a friend and say, "
                    "&ldquo;This part right "
                    "here.&rdquo;</p>\r\n"
                    "<p>The first sampled words heard on "
                    "DJ Shadow&rsquo;s forthcoming album "
                    "Action Adventure, his</p>\r\n"
                    "<p>seventh solo LP, are &ldquo;all "
                    "my records and tapes.&rdquo; In "
                    "fact, they are some of the only "
                    "words heard</p>\r\n"
                    "<p>on what is almost an entirely "
                    "instrumental release; they&rsquo;re "
                    "a kind of thesis statement for "
                    "an</p>\r\n"
                    "<p>album that has its roots in the "
                    "drifting, destabilizing days of the "
                    "COVID lockdown. Action</p>\r\n"
                    "<p>Adventure is an inward-looking "
                    "project, made by Shadow alone "
                    "without any collaborators.</p>\r\n"
                    "<p>Action Adventure tells the "
                    "listener: &ldquo;This is about my "
                    "relationship to music. My life as a "
                    "collector</p>\r\n"
                    "<p>and curator. All my records and "
                    "tapes, and no one "
                    "else&rsquo;s.&rdquo; The result is "
                    "the most exciting DJ</p>\r\n"
                    "<p>&ldquo;Ozone Scraper&rdquo; kicks "
                    "off the album without preamble, just "
                    "big, propulsive drums. It has "
                    "the</p>\r\n"
                    "<p>feeling of transportation, of jet "
                    "engines lifting the passenger "
                    "somewhere impossible. One of "
                    "his</p>\r\n"
                    "<p>favorites on Action Adventure, "
                    "&ldquo;Ozone Scraper&rdquo; is an "
                    "invitation to strap in and also sets "
                    "the</p>\r\n"
                    "<p>table for the kind of deep "
                    "listening Shadow loves. The advanced "
                    "listeners will marvel at the "
                    "kind</p>\r\n"
                    "<p>of &ldquo;special effects,&rdquo; "
                    "as Shadow puts it, that went into "
                    "creating the elusive synth noises. "
                    "Like a</p>\r\n"
                    "<p>child marveling at a summer "
                    "blockbuster, you wonder: How did he "
                    "do this?</p>\r\n"
                    "<p>&ldquo;You Played Me&rdquo; is a "
                    "miracle that happened by pure "
                    "chance. Inspired by the eBay "
                    "tapes,</p>\r\n"
                    "<p>Shadow created a beat that had "
                    "the crackling percussion and "
                    "burbling synths of classic "
                    "&lsquo;80s</p>\r\n"
                    "<p>R&amp;B, like a lost Ren&eacute; "
                    "and Angela hit. &ldquo;I loaded up "
                    "the instrumental and looped it on "
                    "my</p>\r\n"
                    "<p>computer to play forever,&rdquo; "
                    "he explains. Then he started "
                    "auditioning records in his "
                    "collection that</p>\r\n"
                    "<p>he hadn&rsquo;t processed "
                    "yet&mdash;an experiment to find the "
                    "vocal in the haystack. &ldquo;I "
                    "dropped the needle</p>\r\n"
                    "<p>on an acapella of a really "
                    "obscure R&amp;B 12-inch from around "
                    "&lsquo;84, &lsquo;85 and I thought, "
                    "&lsquo;That</p>\r\n"
                    "<p>actually works.&rsquo;&rdquo; "
                    "Still, he wasn&rsquo;t entirely "
                    "sure, and he sent the song to his "
                    "label as a reference</p>\r\n"
                    "<p>for a potential "
                    "collaboration&hellip; whoever it was "
                    "needed to have this sound, Shadow "
                    "told them.</p>\r\n"
                    "<p>After a few weeks, though, he "
                    "realized he already had what he "
                    "needed&mdash;the acapella was</p>\r\n"
                    "<p>correct. &ldquo;It's an example "
                    "of one of my favorite aspects of the "
                    "music I make, which is just "
                    "100%</p>\r\n"
                    "<p>serendipity,&rdquo; he says. "
                    "&ldquo;There are a thousand records "
                    "sitting next to me that aren't going "
                    "to work;</p>\r\n"
                    "<p>the right record got put on at "
                    "the right moment to change the "
                    "course of my album. It&rsquo;s one "
                    "of my</p>\r\n"
                    "<p>favorite songs I&rsquo;ve ever "
                    "made.&rdquo;</p>\r\n"
                    "<p>The album has a cinematic "
                    "quality, especially in its "
                    "sequencing. The last quarter, from "
                    "&ldquo;Fleeting</p>\r\n"
                    "<p>Youth&rdquo; through "
                    "&ldquo;Reflecting Pool&rdquo; and "
                    "&ldquo;Forever Changed,&rdquo; is as "
                    "cathartic as a well-told story, "
                    "and</p>\r\n"
                    "<p>ends on a triumphant note, with "
                    "&ldquo;She&rsquo;s Evolving.&rdquo; "
                    "The oldest recording on Action "
                    "Adventure, it&rsquo;s</p>\r\n"
                    "<p>another happy accident that "
                    "nearly didn&rsquo;t make the album. "
                    "Initially Shadow doubted including "
                    "a</p>\r\n"
                    "<p>song from the cursed early days "
                    "of lockdown and planned to end the "
                    "album with a much sadder</p>\r\n"
                    "<p>track. But it didn&rsquo;t sit "
                    "right and he kept returning to "
                    "&ldquo;She&rsquo;s "
                    "Evolving&rdquo;&mdash;until it "
                    "stuck.&nbsp;</p>\r\n"
                    "<p>The title Action Adventure evokes "
                    "the halcyon days of video store "
                    "browsing, and it&rsquo;s fitting "
                    "that shadow LP in years.</p>\r\n"
                    "<p>In March 2020, Shadow narrowly "
                    "escaped from the European tour for "
                    "his double-disc release</p>\r\n"
                    "<p>Our Pathetic Age, landing back "
                    "home in the Bay Area as the world "
                    "shuttered. He watched his</p>\r\n"
                    "<p>two teenage daughters endure "
                    "homeschooling and tried to process "
                    "his own emotions about the</p>\r\n"
                    "<p>tour cut short; an album he was "
                    "proud of that didn&rsquo;t get to "
                    "have its moment in public. &ldquo;It "
                    "was a</p>\r\n"
                    "<p>very intense time,&rdquo; he "
                    "recalls. For over a year, he "
                    "couldn&rsquo;t imagine making any "
                    "new music, or</p>\r\n"
                    "<p>even listening to any "
                    "contemporary music. &ldquo;It was as "
                    "if the music coming out was "
                    "somehow</p>\r\n"
                    "<p>tainted by the craziness we were "
                    "living through, the chaos and "
                    "upheaval. I needed to</p>\r\n"
                    "<p>temporarily wallow in a bit of "
                    "nostalgia, and not necessarily songs "
                    "I knew&rdquo;&mdash;but music from "
                    "a</p>\r\n"
                    "<p>time period that felt less "
                    "fraught.&nbsp;</p>\r\n"
                    "<p>Around this same time, in 2021, a "
                    "friend directed Shadow to an eBay "
                    "auction of about 200 tapes</p>\r\n"
                    "<p>that were recorded off the radio "
                    "from a mix station that serviced the "
                    "Baltimore-D.C. area in the</p>\r\n"
                    "<p>&lsquo;80s. During that time, he "
                    "taped Bay Area radio mixes and was "
                    "able to get his hands on New</p>\r\n"
                    "<p>York mixes as well, but these "
                    "mixes felt entirely unique in their "
                    "blend of dance music, R&amp;B, "
                    "and</p>\r\n"
                    "<p>early hip-hop. They had a "
                    "youthful, &ldquo;we know no "
                    "limits&rdquo; quality that "
                    "invigorated Shadow at a time</p>\r\n"
                    "<p>when he desperately needed "
                    "it.</p>\r\n"
                    "<p>Shadow began work on Action "
                    "Adventure on January 1, 2022, and "
                    "the early tracks pushed him</p>\r\n"
                    "<p>further into his compositional "
                    "bag. &ldquo;I didn't want to write "
                    "music that was formatted for "
                    "vocalists. I</p>\r\n"
                    "<p>wanted to write music that flexed "
                    "different energies,&rdquo; he says. "
                    "Though he isn&rsquo;t "
                    "classically</p>\r\n"
                    "<p>trained, he asked himself "
                    "questions like &ldquo;which chord "
                    "progression would be most natural "
                    "here,</p>\r\n"
                    "<p>and which would be least "
                    "predictable?&rdquo; and worked "
                    "according to the inner logic that "
                    "felt right to</p>\r\n"
                    "<p>him. His rule for the record was "
                    "simple: no compromises. "
                    "&ldquo;I&rsquo;m entering my fourth "
                    "decade doing</p>\r\n"
                    "<p>this&mdash;what do I want to "
                    "represent? I know that I don&rsquo;t "
                    "want to only make beats for "
                    "some</p>\r\n"
                    "<p>potential vocalist that "
                    "I&rsquo;ve never met, and who may "
                    "not share my vision. I want this "
                    "record to</p>\r\n"
                    "<p>stand or fall on my own "
                    "credentials.&rdquo;</p>",
                    "city": None,
                    "country": "United States",
                    "countryCode": "US",
                    "createFacebookEvent": True,
                    "eventDisplay": 1,
                    "events": [],
                    "externalPartnerID": {},
                    "fanClubs": [],
                    "keywords": [],
                    "links": [
                        {
                            "link_href": "http://www.myspace.com/djshadow",
                            "link_id": "947192",
                            "link_target": "_none",
                            "link_title": "Myspace",
                            "link_type_id": "8",
                        },
                        {
                            "link_href": "http://www.amazon.com/DJ-Shadow/e/B000AQ6OXG/ref=ntt_mus_dp_pel",
                            "link_id": "947193",
                            "link_target": "_none",
                            "link_title": "Amazon",
                            "link_type_id": "11",
                        },
                        {
                            "link_href": "https://twitter.com/djshadow",
                            "link_id": "947194",
                            "link_target": "_none",
                            "link_title": "Twitter",
                            "link_type_id": "13",
                        },
                        {
                            "link_href": "http://www.youtube.com/djshadow",
                            "link_id": "947195",
                            "link_target": "_none",
                            "link_title": "Youtube VEVO",
                            "link_type_id": "14",
                        },
                        {
                            "link_href": "http://www.youtube.com/watch?v=ploXN6YFweE",
                            "link_id": "947196",
                            "link_target": "_none",
                            "link_title": "Featured Video",
                            "link_type_id": "15",
                        },
                        {
                            "link_href": "https://www.facebook.com/djshadow/",
                            "link_id": "947197",
                            "link_target": "_none",
                            "link_title": "Facebook Page",
                            "link_type_id": "12",
                        },
                    ],
                    "locale": "en-US",
                    "localeAvailable": {"1": "en-US"},
                    "majorCategory1Id": "2",
                    "majorCategory2Id": None,
                    "majorCategory2Text": None,
                    "majorCategory3Id": None,
                    "majorCategory3Text": None,
                    "majorCategoryText": "Music",
                    "majorCategoryUrl": "https://www.axs.com/browse/music",
                    "major_category_id": "2",
                    "media": {
                        "19": {
                            "file_name": "https://images.discovery-prod.axs.com/2023/10/dj-shadow_10-30-23_19_653fe2f8d8e59.jpg",
                            "height": 399,
                            "media_id": 4801450,
                            "width": 678,
                        },
                        "20": {
                            "file_name": "https://images.discovery-prod.axs.com/2023/10/dj-shadow_10-30-23_20_653fe2f98c166.jpg",
                            "height": 564,
                            "media_id": 4801454,
                            "width": 564,
                        },
                        "24": {
                            "file_name": "https://images.discovery-prod.axs.com/2023/10/dj-shadow_10-30-23_24_653fe2f9e2128.jpg",
                            "height": 322,
                            "media_id": 4801456,
                            "width": 322,
                        },
                        "5": {
                            "file_name": "https://images.discovery-prod.axs.com/2023/10/dj-shadow_10-30-23_5_653fe2f917ba7.jpg",
                            "height": 187,
                            "media_id": 4801451,
                            "width": 318,
                        },
                        "6": {
                            "file_name": "https://images.discovery-prod.axs.com/2023/10/dj-shadow_10-30-23_6_653fe2f94d25d.jpg",
                            "height": 140,
                            "media_id": 4801452,
                            "width": 238,
                        },
                        "7": {
                            "file_name": "https://images.discovery-prod.axs.com/2023/10/dj-shadow_10-30-23_7_653fe2f9c3c9e.jpg",
                            "height": 220,
                            "media_id": 4801455,
                            "width": 220,
                        },
                        "88": {
                            "file_name": "https://images.discovery-prod.axs.com/2023/10/dj-shadow_10-30-23_88_653fe2fa1de08.jpg",
                            "height": 628,
                            "media_id": 4801457,
                            "width": 1200,
                        },
                        "9": {
                            "file_name": "https://images.discovery-prod.axs.com/2023/10/dj-shadow_10-30-23_9_653fe2f969062.jpg",
                            "height": 79,
                            "media_id": 4801453,
                            "width": 134,
                        },
                    },
                    "minorCategory1Id": "17",
                    "minorCategory2Id": None,
                    "minorCategory2Text": None,
                    "minorCategory3Id": None,
                    "minorCategory3Text": None,
                    "minorCategoryId": "17",
                    "minorCategoryText": "Dance / Electronic",
                    "minorCategoryUrl": "https://www.axs.com/browse/music/dance-electronic",
                    "name": "DJ Shadow",
                    "performerId": "104140",
                    "shortBio": None,
                    "state": None,
                    "tier": None,
                    "under21": False,
                    "url": "https://www.axs.com/artists/104140/dj-shadow-tickets",
                    "urlSlug": "dj-shadow-tickets",
                }
            ],
            "performerIds": ["104140", "1106277"],
            "supportingActs": [
                {
                    "active": True,
                    "aliases": None,
                    "bio": "Miguel Oliveira aka "
                    "&ldquo;Holly&rdquo; is a 24 "
                    "year-old musician from Portugal. "
                    "After only a few years in music, "
                    "the young bass-artist had "
                    "already toured the US, "
                    "Australia, Europe, Asia and "
                    "South Africa, with releases on "
                    "some of the biggest independent "
                    "labels including Buygore, "
                    "Fool&rsquo;s Gold Records, Dim "
                    "Mak Records and Monstercat. "
                    "Holly&rsquo;s music quickly "
                    "caught the attention from "
                    "dance-music mainstays, with "
                    "co-signs from The Chainsmokers, "
                    "Jauz, Bassnectar, Borgore, Steve "
                    "Aoki, David Guetta, Martin "
                    "Garrix and is considered by "
                    "music platforms, like DJ Mag and "
                    "Run The Trap as &lsquo;one of "
                    "the top producers of "
                    "2017&rsquo;. Holly has also won "
                    "the first A-Trak Goldie Awards "
                    "Beat Battle.",
                    "city": None,
                    "country": None,
                    "countryCode": None,
                    "createFacebookEvent": True,
                    "eventDisplay": 1,
                    "events": [],
                    "externalPartnerID": {},
                    "fanClubs": [],
                    "keywords": [],
                    "links": [
                        {
                            "link_href": "https://www.instagram.com/beatsbyholly/?hl=en",
                            "link_id": 1088236,
                            "link_target": "_none",
                            "link_title": "Instagram ",
                            "link_type_id": "49",
                        },
                        {
                            "link_href": "https://www.facebook.com/beatsbyholly/",
                            "link_id": 1088234,
                            "link_target": "_none",
                            "link_title": "Facebook Page",
                            "link_type_id": "12",
                        },
                        {
                            "link_href": "https://twitter.com/beatsbyholly?lang=en",
                            "link_id": 1088235,
                            "link_target": "_none",
                            "link_title": "Twitter ",
                            "link_type_id": "13",
                        },
                        {
                            "link_href": "https://beatsbyholly.com/",
                            "link_id": 1088237,
                            "link_target": "_none",
                            "link_title": "Official " "Website",
                            "link_type_id": "8",
                        },
                    ],
                    "locale": "en-US",
                    "localeAvailable": {"1": "en-US"},
                    "majorCategory1Id": "2",
                    "majorCategory2Id": None,
                    "majorCategory2Text": None,
                    "majorCategory3Id": None,
                    "majorCategory3Text": None,
                    "majorCategoryText": "Music",
                    "majorCategoryUrl": "https://www.axs.com/browse/music",
                    "major_category_id": "2",
                    "media": {},
                    "minorCategory1Id": "17",
                    "minorCategory2Id": None,
                    "minorCategory2Text": None,
                    "minorCategory3Id": None,
                    "minorCategory3Text": None,
                    "minorCategoryId": "17",
                    "minorCategoryText": "Dance / Electronic",
                    "minorCategoryUrl": "https://www.axs.com/browse/music/dance-electronic",
                    "name": "Holly",
                    "performerId": "1106277",
                    "shortBio": None,
                    "state": None,
                    "tier": None,
                    "under21": False,
                    "url": "https://www.axs.com/artists/1106277/holly-tickets",
                    "urlSlug": "holly-tickets",
                }
            ],
        },
        "axsTicketedEvent": True,
        "bio": "<div><span>&ldquo;What&rsquo;s inherently significant about the new "
        "record are all of the new influences I&rsquo;ve taken in. I wanted to "
        "have more on offer than just being &lsquo;the sample "
        "guy.&rsquo;&rdquo; So says Josh Davis, aka DJ Shadow, of his fifth "
        "studio album, The Mountain Will Fall.</span></div>\r\n"
        "<div>&nbsp;</div>\r\n"
        "<div>&nbsp;The restlessness and adventure that has defined "
        "Shadow&rsquo;s career thus far is in evidence again on this album. "
        "While the presence of Mass Appeal label-mates Run The Jewels may "
        "comfort some, it&rsquo;s the addition of UK jazzer Matthew Halsall "
        "and Berlin-based keyboard genius Nils Frahm that suggests a "
        "departure, without ever forsaking a hip-hop aesthetic. Eschewing old "
        "studio aids like the MPC and Pro Tools, Davis was energized by new "
        "working methods. &ldquo;I made most of it on Ableton Live,&rdquo; he "
        "says. &ldquo;To me, it&rsquo;s a new instrument and a new way of "
        "making music. There are three or four songs which really don&rsquo;t "
        "have any samples to speak of, and that in itself is somewhat of a "
        "departure.&rdquo;</div>\r\n"
        "<div>&nbsp;</div>\r\n"
        "<div>Of course, the reason why DJ Shadow was indeed &lsquo;the sample "
        "guy&rsquo; was forged in a lengthy career that begat the lauded debut "
        "album Endtroducing&hellip;.. (later this year he will celebrate its "
        "20th anniversary), helped revive the careers of luminaries like David "
        "Axelrod as well as numerous killer compilations that helped redefine "
        "the digger&rsquo;s art. He is the DJ&rsquo;s DJ.</div>\r\n"
        "<div>&nbsp;</div>\r\n"
        "<div>Spotted releasing early productions on his own Solesides, Shadow "
        "was signed to the influential Mo&rsquo; Wax in the UK by James "
        "Lavelle. The pair collaborated on U.N.K.L.E., working with Thom Yorke "
        "and the Beastie&rsquo;s Mike D on a debut LP, Psyence Fiction, that "
        "sold over 1m. copies (the Mo&rsquo; Wax duo feature in a forthcoming "
        "documentary Artist &amp; Repertoire). Over the next decade, DJ Shadow "
        "released another three albums, including the acclaimed The Private "
        "Press, 2011&rsquo;s The Less You Know, The Better and the adventurous "
        "The Outsider, which included the hit-that-never-was, This Time, "
        "featuring the string arrangements of Wil Malone.</div>\r\n"
        "<div>&nbsp;</div>\r\n"
        "<div>His original material has regularly been disrupted and energized "
        "by tours that have showcased his quiet showmanship, as on Live From "
        "The Shadowsphere, described by Beatport as one of the top ten DJ "
        "shows ever. In&nbsp; 2014 he once again teamed up with Cut Chemist "
        "(the pair made DJ&rsquo;s delights Brainfreeze and Product Placement "
        "together) to create a live set entirely constructed out of Afrika "
        "Bambaataa&rsquo;s mammoth record collection to predictably ecstatic "
        "reviews.</div>\r\n"
        "<div>&nbsp;</div>\r\n"
        "<div>&ldquo;I try to force myself to change things on every record, "
        "and it&rsquo;s why I feel that my music will always change,&rdquo; "
        "says Josh. &ldquo;It&rsquo;s a bit preposterous to use old samplers "
        "when there&rsquo;s so much more you can do with the new ones. "
        "We&rsquo;d never have the amazing music that Miles Davis and Herbie "
        "Hancock did in the 1970s if they&rsquo;d refused to use new "
        "synthesizers, which really defined what they did. It&rsquo;s people "
        "like that that I look to for inspiration.&rdquo; Now listen to the "
        "sound of a mountain falling.</div>\r\n"
        "<p>&nbsp;</p>",
        "clonedEventId": None,
        "createdUTC": "2023-10-27T00:42:18",
        "currency": "USD",
        "currencySymbol": "$",
        "dateOnly": False,
        "description": None,
        "doorDateTime": "2024-02-08T19:00:00",
        "doorDateTimeUTC": "2024-02-09T00:00:00",
        "doorPrice": "$29.50-$34.00",
        "doorPriceHigh": "$34.00",
        "doorPriceLow": "$29.50",
        "eventDateTime": "2024-02-08T20:00:00",
        "eventDateTimeISO": "2024-02-08T20:00:00-05:00",
        "eventDateTimeUTC": "2024-02-09T01:00:00",
        "eventDateTimeZone": "America/New_York",
        "eventDateTimeZoneAbbr": "EST",
        "eventId": "510037",
        "eventRedirect": {
            "enabled": False,
            "url": "https://www.axs.com/festivals/510037",
        },
        "expirationDateTime": None,
        "expirationDateTimeUTC": None,
        "fanSkins": [],
        "hideEventTime": False,
        "keywords": [],
        "links": [
            {
                "link_href": "https://www.facebook.com/djshadow/",
                "link_id": "947197",
                "link_target": "_none",
                "link_title": "Facebook Page",
                "link_type_id": "12",
            },
            {
                "link_href": "https://twitter.com/djshadow",
                "link_id": "947194",
                "link_target": "_none",
                "link_title": "Twitter",
                "link_type_id": "13",
            },
            {
                "link_href": "http://www.youtube.com/watch?v=ploXN6YFweE",
                "link_id": "947196",
                "link_target": "_none",
                "link_title": "Featured Video",
                "link_type_id": "15",
            },
        ],
        "livestreamEnabled": False,
        "livestreamUrl": None,
        "locale": "en-US",
        "localeAvailable": {"1": "en-US"},
        "majorCategoryId1": "2",
        "majorCategoryId1Label": "Music",
        "majorCategoryId2": None,
        "majorCategoryId2Label": None,
        "majorCategoryId3": None,
        "majorCategoryId3Label": None,
        "media": {
            "1": {
                "file_name": "https://images.discovery-prod.axs.com/2023/10/uploadedimage-jpg_653fd17c2f860.jpg",
                "height": 187,
                "media_id": "4801381",
                "width": 318,
            },
            "17": {
                "file_name": "https://images.discovery-prod.axs.com/2023/10/uploadedimage_653fd17bdfb28.jpg",
                "height": 399,
                "media_id": "4801380",
                "width": 678,
            },
            "18": {
                "file_name": "https://images.discovery-prod.axs.com/2023/10/uploadedimage_653fd17c99245.jpg",
                "height": 564,
                "media_id": "4801384",
                "width": 564,
            },
            "2": {
                "file_name": "https://images.discovery-prod.axs.com/2023/10/uploadedimage-jpg_653fd17c5db05.jpg",
                "height": 140,
                "media_id": "4801382",
                "width": 238,
            },
            "23": {
                "file_name": "https://images.discovery-prod.axs.com/2023/10/uploadedimage-jpg_653fd17d01aac.jpg",
                "height": 322,
                "media_id": "4801386",
                "width": 322,
            },
            "3": {
                "file_name": "https://images.discovery-prod.axs.com/2023/10/uploadedimage-jpg_653fd17cc38ce.jpg",
                "height": 220,
                "media_id": "4801385",
                "width": 220,
            },
            "4": {
                "file_name": "https://images.discovery-prod.axs.com/2023/10/uploadedimage-jpg_653fd17c7c30f.jpg",
                "height": 79,
                "media_id": "4801383",
                "width": 134,
            },
            "86": {
                "file_name": "https://images.discovery-prod.axs.com/2023/10/uploadedimage_653fd17d29afe.jpg",
                "height": 628,
                "media_id": "4801387",
                "width": 1200,
            },
        },
        "minorCategoryId1": "17",
        "minorCategoryId1Label": "Dance / Electronic",
        "minorCategoryId2": None,
        "minorCategoryId2Label": None,
        "minorCategoryId3": None,
        "minorCategoryId3Label": None,
        "modifiedUTC": "2024-01-12T19:37:04",
        "onsaleDateTime": "2023-11-03T10:00:00",
        "onsaleDateTimeTimeZone": "America/New_York",
        "onsaleDateTimeUTC": "2023-11-03T14:00:00",
        "onsaleDateTimeZoneAbbr": "EST",
        "presaleDateTime": None,
        "presaleDateTimeTimeZone": "America/New_York",
        "presaleDateTimeUTC": None,
        "presaleEndDateTime": None,
        "presaleEndDateTimeTimeZone": "America/New_York",
        "presaleEndDateTimeUTC": None,
        "private": False,
        "promoters": None,
        "publishStatus": 1,
        "relatedMedia": {
            "1": {
                "file_name": "https://images.discovery-prod.axs.com/2023/10/uploadedimage-jpg_653fd17c2f860.jpg",
                "height": 187,
                "media_id": "4801381",
                "width": 318,
            },
            "102": {
                "file_name": "https://s.axs.com/axs/bundles/aegaxs/images/defaults/1/1_860_400.png",
                "height": "400",
                "media_id": 0,
                "width": "860",
            },
            "17": {
                "file_name": "https://images.discovery-prod.axs.com/2023/10/uploadedimage_653fd17bdfb28.jpg",
                "height": 399,
                "media_id": "4801380",
                "width": 678,
            },
            "18": {
                "file_name": "https://images.discovery-prod.axs.com/2023/10/uploadedimage_653fd17c99245.jpg",
                "height": 564,
                "media_id": "4801384",
                "width": 564,
            },
            "2": {
                "file_name": "https://images.discovery-prod.axs.com/2023/10/uploadedimage-jpg_653fd17c5db05.jpg",
                "height": 140,
                "media_id": "4801382",
                "width": 238,
            },
            "23": {
                "file_name": "https://images.discovery-prod.axs.com/2023/10/uploadedimage-jpg_653fd17d01aac.jpg",
                "height": 322,
                "media_id": "4801386",
                "width": 322,
            },
            "3": {
                "file_name": "https://images.discovery-prod.axs.com/2023/10/uploadedimage-jpg_653fd17cc38ce.jpg",
                "height": 220,
                "media_id": "4801385",
                "width": 220,
            },
            "4": {
                "file_name": "https://images.discovery-prod.axs.com/2023/10/uploadedimage-jpg_653fd17c7c30f.jpg",
                "height": 79,
                "media_id": "4801383",
                "width": 134,
            },
            "81": {
                "file_name": "https://s.axs.com/axs/bundles/aegaxs/images/defaults/1/1_1770_580.png",
                "height": "580",
                "media_id": 0,
                "width": "1770",
            },
            "86": {
                "file_name": "https://images.discovery-prod.axs.com/2023/10/uploadedimage_653fd17d29afe.jpg",
                "height": 628,
                "media_id": "4801387",
                "width": 1200,
            },
        },
        "safetyIcons": [],
        "safetyModalEnabled": False,
        "shortCodeId": None,
        "shortCodeName": None,
        "siteData": {
            "1": "AXS WEB",
            "14": "AEG Presents",
            "289": "Union Transfer",
            "39": "AXS CMS",
            "59": "Bowery Presents",
            "66": "Philadelphia",
            "999": "Public EDP",
        },
        "thirdPartyAccessBarcodeRevealDateTime": None,
        "thirdPartyAccessBarcodeRevealDateTimeTimeZone": None,
        "thirdPartyAccessControlBarcodeEnabled": False,
        "ticketPrice": None,
        "ticketPriceHigh": "$0.00",
        "ticketPriceLow": "$0.00",
        "ticketing": {
            "callToAction": "Get your tickets today!",
            "callToActionEnabled": False,
            "customRefundRequestCTA": None,
            "customRefundRequestText": None,
            "eventUrl": "https://www.axs.com/events/510037/dj-shadow-tickets",
            "listingContiguous": False,
            "listingText": None,
            "status": "Buy Tickets",
            "statusId": 1,
            "ticketLinkExists": True,
            "transferContiguous": False,
            "transferText": None,
            "url": "https://www.axs.com/events/510037/dj-shadow-tickets",
        },
        "tier": None,
        "title": {
            "eventTitle": '<a href="/artists/104140/dj-shadow-tickets">DJ '
            "Shadow</a>",
            "eventTitleText": "DJ Shadow",
            "headliners": '<a href="/artists/104140/dj-shadow-tickets">DJ '
            "Shadow</a>",
            "headlinersText": "DJ Shadow",
            "presentedBy": "<a "
            'href="https://www.axs.com/artists/104140/dj-shadow-tickets">Action '
            "Adventure Tour</a>",
            "presentedByText": "Action Adventure Tour",
            "supporting": '<a href="/artists/1106277/holly-tickets">Holly</a>',
            "supportingText": "Holly",
            "tour": None,
        },
        "tour": [],
        "venue": {
            "active": True,
            "ada": None,
            "address": "1026 Spring Garden Street",
            "address_line": "1026 Spring Garden Street, Philadelphia, PA 19123",
            "alias": None,
            "axs_ticketed_venue": True,
            "boxOffice": None,
            "city": "Philadelphia",
            "country": "United States",
            "countryCode": "US",
            "currency": "USD",
            "directions": None,
            "distanceKilometers": None,
            "distanceMiles": None,
            "eventDisplay": 1,
            "keywords": [],
            "latitude": "39.961406",
            "links": [
                {
                    "link_href": "https://www.facebook.com/UnionTransfer",
                    "link_id": 1108489,
                    "link_target": "_blank",
                    "link_title": "Facebook Page",
                    "link_type_id": "24",
                }
            ],
            "locale": "en-US",
            "localeAvailable": {"1": "en-US"},
            "longitude": "-75.155349",
            "media": {},
            "parking": None,
            "phone1": None,
            "phone2": None,
            "postalCode": "19123",
            "safetyIconUrl": None,
            "safetyIcons": [],
            "safetyModalEnabled": False,
            "skinCode": None,
            "state": "PA",
            "tier": None,
            "timezone": "America/New_York",
            "title": "Union Transfer",
            "url": "https://www.axs.com/venues/126150/union-transfer-philadelphia-tickets",
            "urlSlug": "union-transfer-philadelphia-tickets",
            "venueId": "126150",
            "venueUrl": "http://www.utphilly.com/",
        },
        "waitingRoomTime": 0,
    }


@pytest.fixture(scope="module")
def r5_event_card():
    return """
  <span class="rhp-events-list-separator-month d-flex"><span>September 2024</span></span>
  <div class="col-12 eventWrapper rhpSingleEvent py-4 px-0">
    <div class="row g-0">
      <!-- Event left -->
      <div class="col-12 col-md-3">
        <div class="row g-0">
          <div class="col-sm-12 p-0">
          <!-- RHP Event Thumbnail -->
            <div class="rhp-event-thumb">
              <a class="url" href="https://r5productions.com/event/waxahatchee/the-fillmore/philadelphia-pennsylvania/" title="Waxahatchee" rel="bookmark">
                <div class="eventDateListTop">
                  <div id="eventDate" class="mb-0 eventMonth singleEventDate text-uppercase ">
                      Sat, Sept 07
                  </div>
                </div>
                <div class="rhp-events-event-image">
                  <img src="https://r5productions.com/wp-content/uploads/2024/01/MAIN-Waxahatchee-by-Molly-Matalon-9.jpg" class="eventListImage" title="Waxahatchee">
                </div>
              </a>
            </div> <!-- End RHP Event Thumbnail-->
          </div> <!-- end event image container -->
        </div>
      </div><!-- end event left col-sm-3 -->
      <!-- Event center -->
      <div class="col-12 col-md-6">
        <div class="col-12 rhp-event-info">
          <div class="row g-0">
          <!-- Low Ticket alert-->
            <div class="col-12 belowLowTicketSection p-2">
            <!-- RHP Event Tagline -->
              <div class="col-12 px-0 mb-md-2 eventTagLine">
                WXPN 88.5 Welcomes
              </div>
              <!-- end event tag line -->
              <div class="col-12 px-0 eventTitleDiv">
                <!-- RHP Event Header -->
                <a id="eventTitle" class="url" href="https://r5productions.com/event/waxahatchee/the-fillmore/philadelphia-pennsylvania/" title="Waxahatchee" rel="bookmark">
                  <h2 class="font1by25 font1By5remMD marginBottom3PX   lineHeight12 font1By75RemSM font1By5RemXS mt-md-0 mb-md-2">
                    Waxahatchee
                  </h2>
                </a>
              </div><!-- end Event header-->
              <!-- RHP Event Subheader -->
              <div class="col-12 px-0 ">
                <h4 id="evSubHead" class="eventSubHeader font0by875 lineHeight12 font0By9375remMD font1remSM mt-md-0 mb-md-2">
                  Tim Heidecker, Gladie
                </h4>
              </div>
              <!-- end event sub header-->
              <!-- Event Details -->
              <div class="col-12 px-0 rhpEventDetails">
                <div class="row g-0">
                  <div class="col-12">
                    <div class="eventDateDetails mt-md-0 mb-md-2">
                      <div class="eventsColor eventDoorStartDate d-block">
                        <span class="font0by75 fontWeight500 lineHeight15">
                          Doors: 7pm | Show: 8pm
                        </span>
                      </div><!-- end event doors date  -->
                    </div><!-- end col-sm-12 -->
                  </div> <!-- col-12-->
                  <div class="col-12 eventsVenueDiv " id="eventVenueId-10753">
                    <!-- Events Venue -->
                    <div class="d-inline-block eventVenue ">
                      <i class="rhp-events-icon location"></i>
                      <div class="rhpVenueContent">
                        <a class="venueLink" href="https://r5productions.com/venue/the-fillmore/" title="The Fillmore">The Fillmore</a>
                      </div>
                    </div><!-- end venue -->
                  </div><!-- end col-12 -->
                  <div class="col-12 eventsRoomDiv d-none" id="eventRoomId-">
                  </div><!-- end col-12 -->
                  <!-- Display pre sale date time has not passed -->
                </div><!-- end row -->
              </div><!-- end event details-->
            </div><!-- below low ticket alert section -->
          </div><!-- end row -->
        </div><!-- end rhp-event-info-->
      </div><!-- end event center col-sm-3-->
      <!-- Event right-->
      <div class="col-12 col-md-3 text-center">
        <div class="row g-0">
          <div class="col-12 rhp-event-list-cta mt-md-0">
            <span id="ctaspan-11341" class="rhp-event-cta on-sale">
              <a class="btn btn-primary btn-md d-block w-100" target="_blank" rel="external" title="Buy Tickets" href="https://www.ticketmaster.com/event/02005F8FBA825713">Buy Tickets</a>
            </span>
            <div class="multiple_ctas d-block">
              <div class="rhp-event-rsvp-cta-box rsvp mt-2">
                <a target="_blank" class="btn btn-primary btn-md d-block w-100 secondary" href="https://www.facebook.com/events/301692146204786/">RSVP</a>
              </div><!-- /.rhp-event-secondary-cta-box -->
            </div>
          </div><!-- .rhp-event-single-cta -->
          <!-- rhp Event More Info Button -->
          <div class="col-12 mt-2 text-center eventMoreInfo">
            <a class="btn btn-md d-block w-100" href="https://r5productions.com/event/waxahatchee/the-fillmore/philadelphia-pennsylvania/" rel="bookmark">
              More Info
            </a>
          </div>
        </div><!-- end row -->
      </div><!-- end event right col-sm-3-->
    </div>
  </div>
    """

@pytest.fixture()
def collective_4333_show():
    return """
    <article class="eventlist-event eventlist-event--upcoming eventlist-event--hasimg eventlist-hasimg">
<a class="eventlist-column-thumbnail content-fill" data-animation-role="image" href="/gigs/9h8fsnypml7ctwf-trh7z-dd543-amxdn-252lm-xne6w-cbzer-z9bb6-9n83k-5dhfr-ngdfl-2lpfy-j9b3k-k6nge-kjctx-lft73-et582-m7pdw-part7-g5k36-2mbjz-34677-6ap5r-cbbjc-z4pmz">
<img alt="" data-image="https://images.squarespace-cdn.com/content/v1/5bb7d41c92441b71580fd948/1735571371131-LG5UXJ3D166MN7DSAZ2Z/e4e3de13-00e8-4ed9-87c5-20adc9e0c447.jpg" data-image-dimensions="768x768" data-image-focal-point="0.5,0.5" data-load="false" data-loader="sqs" data-src="https://images.squarespace-cdn.com/content/v1/5bb7d41c92441b71580fd948/1735571371131-LG5UXJ3D166MN7DSAZ2Z/e4e3de13-00e8-4ed9-87c5-20adc9e0c447.jpg" decoding="async" elementtiming="nbf-events-stacked" height="768" loading="lazy" sizes="(max-width: 639px)150vw,52.5vw" src="https://images.squarespace-cdn.com/content/v1/5bb7d41c92441b71580fd948/1735571371131-LG5UXJ3D166MN7DSAZ2Z/e4e3de13-00e8-4ed9-87c5-20adc9e0c447.jpg" srcset="https://images.squarespace-cdn.com/content/v1/5bb7d41c92441b71580fd948/1735571371131-LG5UXJ3D166MN7DSAZ2Z/e4e3de13-00e8-4ed9-87c5-20adc9e0c447.jpg?format=100w 100w, https://images.squarespace-cdn.com/content/v1/5bb7d41c92441b71580fd948/1735571371131-LG5UXJ3D166MN7DSAZ2Z/e4e3de13-00e8-4ed9-87c5-20adc9e0c447.jpg?format=300w 300w, https://images.squarespace-cdn.com/content/v1/5bb7d41c92441b71580fd948/1735571371131-LG5UXJ3D166MN7DSAZ2Z/e4e3de13-00e8-4ed9-87c5-20adc9e0c447.jpg?format=500w 500w, https://images.squarespace-cdn.com/content/v1/5bb7d41c92441b71580fd948/1735571371131-LG5UXJ3D166MN7DSAZ2Z/e4e3de13-00e8-4ed9-87c5-20adc9e0c447.jpg?format=750w 750w, https://images.squarespace-cdn.com/content/v1/5bb7d41c92441b71580fd948/1735571371131-LG5UXJ3D166MN7DSAZ2Z/e4e3de13-00e8-4ed9-87c5-20adc9e0c447.jpg?format=1000w 1000w, https://images.squarespace-cdn.com/content/v1/5bb7d41c92441b71580fd948/1735571371131-LG5UXJ3D166MN7DSAZ2Z/e4e3de13-00e8-4ed9-87c5-20adc9e0c447.jpg?format=1500w 1500w, https://images.squarespace-cdn.com/content/v1/5bb7d41c92441b71580fd948/1735571371131-LG5UXJ3D166MN7DSAZ2Z/e4e3de13-00e8-4ed9-87c5-20adc9e0c447.jpg?format=2500w 2500w" style="display:block;object-fit:cover; width: 100%; height: 100%; position: absolute; object-position: 50% 50%;" width="768"/>
</a>
<div class="eventlist-column-date">
<div class="eventlist-datetag">
<div class="eventlist-datetag-inner">
<div class="eventlist-datetag-startdate eventlist-datetag-startdate--month">Jan</div>
<div class="eventlist-datetag-startdate eventlist-datetag-startdate--day">9</div>
<div class="eventlist-datetag-status"></div>
</div>
</div>
</div>
<div class="eventlist-column-info">
<h1 class="eventlist-title"><a class="eventlist-title-link" href="/gigs/9h8fsnypml7ctwf-trh7z-dd543-amxdn-252lm-xne6w-cbzer-z9bb6-9n83k-5dhfr-ngdfl-2lpfy-j9b3k-k6nge-kjctx-lft73-et582-m7pdw-part7-g5k36-2mbjz-34677-6ap5r-cbbjc-z4pmz">Le Siren, Reese Florence, and Milk Dud</a></h1>
<ul class="eventlist-meta event-meta" data-animation-role="date">
<li class="eventlist-meta-item eventlist-meta-date event-meta-item">
<time class="event-date" datetime="2025-01-09">Thursday, January 9, 2025</time>
</li>
<li class="eventlist-meta-item eventlist-meta-time event-meta-item">
<span class="event-time-localized">
<time class="event-time-localized-start" datetime="2025-01-09">7:00 PM</time>
<span class="event-datetime-divider"></span>
<time class="event-time-localized-end" datetime="2025-01-09">11:00 PM</time>
</span>
</li>
<li class="eventlist-meta-item eventlist-meta-address event-meta-item">

                Ortlieb's

              <a class="eventlist-meta-address-maplink" href="http://maps.google.com?q=847 North 3rd Street Philadelphia, PA, 19123 United States" target="_blank">(map)</a>
</li>
<li class="eventlist-meta-item eventlist-meta-export event-meta-item">
<a class="eventlist-meta-export-google" href="http://www.google.com/calendar/event?action=TEMPLATE&amp;text=Le%20Siren%2C%20Reese%20Florence%2C%20and%20Milk%20Dud&amp;dates=20250110T000000Z/20250110T040000Z&amp;location=847%20North%203rd%20Street%2C%20Philadelphia%2C%20PA%2C%2019123%2C%20United%20States">Google Calendar</a>
<span class="eventlist-meta-export-divider"></span>
<a class="eventlist-meta-export-ical" href="/gigs/9h8fsnypml7ctwf-trh7z-dd543-amxdn-252lm-xne6w-cbzer-z9bb6-9n83k-5dhfr-ngdfl-2lpfy-j9b3k-k6nge-kjctx-lft73-et582-m7pdw-part7-g5k36-2mbjz-34677-6ap5r-cbbjc-z4pmz?format=ical">ICS</a>
</li>
</ul>
<div class="eventlist-description"><div class="sqs-layout sqs-grid-12 columns-12" data-layout-label="Post Body" data-type="item" id="item-6772b6bd2098ac29f8e0c969"><div class="row sqs-row"><div class="col sqs-col-12 span-12"><div class="sqs-block html-block sqs-block-html" data-block-type="2" data-border-radii='{"topLeft":{"unit":"px","value":0.0},"topRight":{"unit":"px","value":0.0},"bottomLeft":{"unit":"px","value":0.0},"bottomRight":{"unit":"px","value":0.0}}' id="block-37f882eb33d176c9f8d5"><div class="sqs-block-content">
<div class="sqs-html-content">
<p class="" style="white-space:pre-wrap;">21+</p><p class="" style="white-space:pre-wrap;">Doors at 7pm</p><p class="" style="white-space:pre-wrap;">$14.28</p>
</div>
</div></div><div class="sqs-block button-block sqs-block-button" data-block-type="53" id="block-af1ddfa555cd4f728168"><div class="sqs-block-content">
<div class="sqs-block-button-container sqs-block-button-container--left" data-alignment="left" data-animation-role="button" data-button-size="medium" data-button-type="primary">
<a class="sqs-block-button-element--medium sqs-button-element--primary sqs-block-button-element" href="https://link.dice.fm/K5752be02471" target="_blank">
    Get Tickets
  </a>
</div>
</div></div></div></div></div></div>
<a class="eventlist-button sqs-editable-button sqs-button-element--primary" href="/gigs/9h8fsnypml7ctwf-trh7z-dd543-amxdn-252lm-xne6w-cbzer-z9bb6-9n83k-5dhfr-ngdfl-2lpfy-j9b3k-k6nge-kjctx-lft73-et582-m7pdw-part7-g5k36-2mbjz-34677-6ap5r-cbbjc-z4pmz">
          View Event →
        </a>
</div>
<div class="clear"></div>
</article>
    """
