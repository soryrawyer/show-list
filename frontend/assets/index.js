function hidePastShows() {
  // get the list of show elements, and for each element:
  // if date < now, add display: none to the element's style
  let showEls = document.getElementsByClassName("show");
  let now = Date.now();
  for (const showEl of showEls) {
    let date = new Date(parseInt(showEl.getElementsByClassName("date-time")[0].dataset.timestamp) * 1000);
    if (now > date) {
      showEl.style.display = "none";
    }
  }
}

hidePastShows();
