const moment = require("moment");
const pluginRss = require("@11ty/eleventy-plugin-rss");

module.exports = function(eleventyConfig) {

  eleventyConfig.addPlugin(pluginRss);
  eleventyConfig.addPassthroughCopy("assets");
  eleventyConfig.addPassthroughCopy("img");

  eleventyConfig.addFilter("readableDate", (dateObj, format) => {
    return moment(dateObj * 1000).format(format);
  });

  return {
    dir: {
      input: "src",
      output: "public",
    }
  }
}
