# show list

pull HTML from various venue websites and put shows in a sqlite database

## installation

this application uses [uv](https://github.com/astral-sh/uv) for package and environment management. it also uses [npm](https://www.npmjs.com/) to manage the frontend dependencies.

to run:
``` sh
# to update a local sqlite database
uv run show_list
# to update the shows.json file
uv run show_list -l -j > frontend/src/_data/showList.json
# to rebuild the frontend
cd frontend && npx eleventy
```

## how it works
at a high level, this app has two components: a Python program to collect shows from venue and promoter websites and a static site using [eleventy](https://www.11ty.dev/) to display those shows.


### python web scraper
the web scraper is meant to be run locally in a virtual environment. the [`__main__.py` file](./show_list/__main__.py) is the entrypoint when running the program as a python module (i.e. via `python -m show_list`) and is where the command-line arguments are defined. each venue or promoter website should implement the `ShowLister` protocol (defined in [`shows.py`](./show_list/shows.py)), which will return a list of `Show` objects from an HTTP repsonse. these objects are then saved to a local sqlite database.
the program also has a command-line option for writing the shows to a JSON file, which is used as the data for the static site

### frontend
the frontend currently consists of one template: [`frontend/src/index.njk`](./frontend/src/index.njk). this template reads the JSON file created by the python program using 11ty's [global data file](https://www.11ty.dev/docs/data-global/#example) feature and renders HTML for each show. this template can be built by running `npx eleventy` from the `frontend` directory.

### hosting
the frontend is hosted using github pages. currently, the site is built locally, checked into git, and served from the `/public` directory. however, I think it'd be nice to eventually build the site using gitlab's CI/CD runners.

### duplicate shows
one thing this program doesn't do very well is remove duplicate entries for a show. here are some queries that will help find duplicate show entries:

```sql
-- find all entries that have the same URL to buy tickets
select ticket_url, count(*) as c from shows group by 1 having c > 1;
```
